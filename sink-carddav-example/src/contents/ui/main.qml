import QtQuick 2.2
import org.kde.kirigami 2.5 as Kirigami
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

Kirigami.ApplicationWindow {
    id: root

    title: "Sink Contacts"

    width: Kirigami.Units.gridUnit * 25
    height: Kirigami.Units.gridUnit * 15

    pageStack.initialPage: mainPageComponent

    Component {
        id: mainPageComponent

        Kirigami.Page {
            title: i18n("Sink Contacts")

            ColumnLayout {
                spacing: 10
                anchors.fill: parent
                
                Button {
                    Layout.fillWidth: true
                    icon.name: "go-next"
                    text: i18n("create contact")
                    onClicked: {
                        hello.createContact();
                    }
                }

                Button {
                    Layout.fillWidth: true
                    icon.name: "go-next"
                    text: i18n("sync contact")
                    onClicked: {
                        hello.synchContact();
                    }
                }

                Button {
                    Layout.fillWidth: true
                    icon.name: "go-next"
                    text: i18n("count contacts")
                    onClicked: {
                        hello.countContacts();
                    }
                }
            }
        }
    }
}
