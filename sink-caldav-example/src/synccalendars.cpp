#include <sink/store.h>
#include <sink/secretstore.h>
#include <sink/resourcecontrol.h>
#include "synccalendars.h"
#include <QDebug>

#include <QUuid>
#include <KCalCore/Event>
#include <KCalCore/ICalFormat>

SyncCalendar::SyncCalendar()
{
}

SyncCalendar::~SyncCalendar()
{
}

void SyncCalendar::createResource()
{
    auto resource = Sink::ApplicationDomain::CalDavResource::create("account1");

    resource.setProperty("server","https://server-address");
    resource.setProperty("username", "username");
    Sink::SecretStore::instance().insert(resource.identifier(), "*****");
    Store::create(resource).exec().waitForFinished();
    qDebug()<<"***Event CREATED***";
    
    mResourceInstanceIdentifier = resource.identifier();
    
    return;
}

void SyncCalendar::synchCalendar()
{
    //start resourcecontrol
    Sink::ResourceControl::start(mResourceInstanceIdentifier);

    //Sync Calendars
    Sink::SyncScope scope1;
    scope1.setType<Sink::ApplicationDomain::Calendar>();
    scope1.resourceFilter(mResourceInstanceIdentifier);
    Store::synchronize(scope1).exec().waitForFinished();

    //flush
    Sink::ResourceControl::flushMessageQueue(mResourceInstanceIdentifier).exec().waitForFinished();
    qDebug()<<"***FLUSHED***";


    //sync event
    Sink::SyncScope scope;
    scope.setType<Event>();
    Sink::Query q;
    q.setType<Calendar>();
    scope.filter(ApplicationDomain::getTypeName<Calendar>(), {QVariant::fromValue(q)});
    scope.resourceFilter(mResourceInstanceIdentifier);
    Store::synchronize(scope).exec().waitForFinished();

    //flush
    Sink::ResourceControl::flushMessageQueue(mResourceInstanceIdentifier).exec().waitForFinished();
    qDebug()<<"***FLUSHED***";

    return;
}

void SyncCalendar::countEvents()
{        
    const auto calendars = Sink::Store::read<Sink::ApplicationDomain::Calendar>(Sink::Query().resourceFilter(mResourceInstanceIdentifier));
    qDebug()<<"Calendars size : "<<calendars.size();

    // Sink::ResourceControl::flushReplayQueue(mResourceInstanceIdentifier).exec().waitForFinished();

    const auto events = Sink::Store::read<Sink::ApplicationDomain::Event>(Sink::Query().resourceFilter(mResourceInstanceIdentifier));
    qDebug()<<"Events size : "<<events.size();

    Sink::ResourceControl::flushReplayQueue(mResourceInstanceIdentifier);
    return;
}