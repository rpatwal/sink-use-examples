#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include "synccalendars.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("KDE");
    QCoreApplication::setOrganizationDomain("kde.org");
    QCoreApplication::setApplicationName("sinkcalendars");

    QQmlApplicationEngine engine;

    SyncCalendar* hello = new SyncCalendar();
    
    // SignIn* signin = new SignIn();

    // engine.rootContext()->setContextProperty("signin",signin);

    engine.rootContext()->setContextProperty("hello",hello);

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
